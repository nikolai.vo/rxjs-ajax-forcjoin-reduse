

// const { Observable } = require('rxjs');


// const { XMLHttpRequest } = require("xmlhttprequest");

// class AjaxResponse {

//   constructor(statusCode, responseBody) {
//     this.statusCode = statusCode;
//     this.body = responseBody;

//   }
// }

// class AjaxError {
//   constructor(statusCode, message) {
//     this.statusCode = statusCode;
//     this.body = message;
//   }
// }

// class AjaxRequest {
//   constructor(config) {
//     this.url = config.url;
//     this.body = config.body;
//     this.method = config.method;
//     this.headers = config.headers;
//   }
// }

// class Ajax {
//   constructor() { }
//   get(request) {
//     if (request instanceof AjaxRequest) {
//       console.log(request instanceof AjaxRequest)
//       return this.sendRequest({
//         ...request,
//         method: 'GET',
//       })
//     }
//   };
//   post(request) {
//     if (request instanceof AjaxRequest) {
//       return this.sendRequest({
//         ...request,
//         method: 'POST'
//       });
//     }
//   }
//   put(request) {
//     if (request instanceof AjaxRequest) {
//       return this.sendRequest({
//         ...request,
//         method: 'PUT',
//       });
//     }
//   }
//   delete(request) {
//     if (request instanceof AjaxRequest) {
//       return this.sendRequest({
//         ...request,
//         method: 'DELETE',
//       });
//     }
//   }
//   patch(request) {
//     if (request instanceof AjaxRequest) {
//       return this.sendRequest({
//         ...request,
//         method: 'PATCH',
//       });
//     }
//   }
//   sendRequest(request) {

//     let stream = Observable.create(observer => {

//       let xhr = new XMLHttpRequest();
//       xhr.open(request.method, request.url, true);
//       xhr.onload = () => {
//         if (xhr.readyState == 4) {
//           if (xhr.status >= 200 && xhr.status < 300) {
//             let res = new AjaxResponse(
//               xhr.status,
//               JSON.parse(xhr.responseText)
//             );
//             observer.next(request.response);
//             observer.complete();
//           } else {
//             let err = new AjaxError(
//               xhr.status,
//               JSON.parse(xhr.responseText)
//             );
//             observer.error(err);
//           }
//         }
//       }
//       if (request.headers) {
//         Object.keys(request.headers).forEach(function (key) {
//           xhr.setRequestHeader(key, request.headers[key]);
//         })
//       }
//       xhr.send(JSON.stringify(request.body));
//     });
//     stream.subscribe(data => console.log(data));
//   }
// }





// sendRequest(request) {

//   let stream = Observable.create(observer => {
//     console.log(observer)
//     let xhr = new XMLHttpRequest();
//     xhr.open(request.method, request.url, true);
//     xhr.onload = () => {
//       if (xhr.readyState == 4) {
//         if (xhr.status >= 200 && xhr.status < 300) {  // logic error
//           observer.next(new AjaxResponse);
//           observer.complete();
//         } else {

//           observer.error(new AjaxError);
//         }
//       }
//     }
//     if (request.headers) {
//       Object.keys(request.headers).forEach(function (key) {
//         xhr.setRequestHeader(key, request.headers[key]);
//       })
//     }

//     xhr.send(JSON.stringify(request.body));
//   });
//   stream.subscribe(data => console.log(data));
// }
// sendRequest(request){
//   return new Promise(function(resolve, reject){
//     let xhr = new XMLHttpRequest();
//       xhr.open(request.method, request.url, true);
//       xhr.onreadystatechange = function handleRequest(){

//         if (xhr.readyState == 4){
//           if (xhr.status >= 200 && xhr.status < 300){  // logic error
//             let res = new AjaxResponse(
//               xhr.status,
//               JSON.parse(xhr.responseText)
//             );
//             resolve(res);
//           } else {

//             let err = new AjaxError(
//               xhr.status,
//               JSON.parse(xhr.responseText)
//             );
//             reject(err);
//           }
//         }
//       }

//     if(request.headers) {
//       Object.keys(request.headers).forEach(function(key){
//         xhr.setRequestHeader(key,request.headers[key]);
//       })
//     }

//     xhr.send(JSON.stringify(request.body));
//   });
// }
