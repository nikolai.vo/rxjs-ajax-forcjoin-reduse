import * as Rx from 'rxjs';
import { Observable } from 'rxjs';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { merge } from 'rxjs/observable/merge';
import { reduce } from 'rxjs/operator/reduce';
import { concat } from 'rxjs/observable/concat';
import { interval } from 'rxjs/observable/interval';
import { tap, map, switchMap, take, scan, observeOn, throttleTime } from 'rxjs/operators';
import { timer } from 'rxjs/observable/timer';
import { ajax } from 'rxjs/observable/dom/ajax'
import { forkJoin } from 'rxjs/observable/forkJoin';
// const users = Rx.Observable
//   .ajax({
//     url: 'https://rx-microadmin.herokuapp.com/users',
//     method: 'GET',
//     headers: {
//       'Content-Type': 'application/json'
//     }
//   })
//   .map(e => e.response);

// const apps = Rx.Observable.ajax({
//   url: 'https://rx-microadmin.herokuapp.com/apps',
//   method: 'GET',
//   headers: {
//     'Content-Type': 'application/json'
//   }
// }).map(e => e.response);

// const devices = Rx.Observable.ajax({
//   url: 'https://rx-microadmin.herokuapp.com/devices',
//   method: 'GET',
//   headers: {
//     'Content-Type': 'application/json'
//   }
// }).map(e => e.response);
// resultGet.subscribe(([users, apps, devices]) => {
//   const usersResponse = users;
//   const appsResponse = apps;
//   const devicesResponse = devices;
//   console.log(typeof usersResponse)
//   console.log(usersResponse)

//   Object.keys(devicesResponse).map((key) => {
//     let dataDevices = devicesResponse[key].id;
//     return (
//       Object.assign(devicesResponse[key], {
//         apps: appsResponse.filter(items => items.host_id == dataDevices)
//       })

//     );
//   })

//   Object.keys(usersResponse).map((key) => {
//     let dataUsers = usersResponse[key].id;
//     return (
//       Object.assign(usersResponse[key], {
//         devices: devicesResponse.filter(items => items.user_id == dataUsers)
//       })
//     );
//   })
//   Object.keys(usersResponse).map((key) => {
//     let myDev = usersResponse[key].devices;
//     Object.keys(myDev).map((key) => {
//       let mySize = myDev[key].apps
//       // console.log(mySize)
//     })
//     // console.log(size)

//   })

//   let a = JSON.stringify(usersResponse, null, 2)
//   // console.log(a)
//   return a;
// });

const request = forkJoin(
  ajax('https://rx-microadmin.herokuapp.com/users'),
  ajax('https://rx-microadmin.herokuapp.com/apps'),
  ajax('https://rx-microadmin.herokuapp.com/devices')
)
// let myResult = [];
// const resultGet = Rx.Observable.forkJoin(users, apps, devices);
request.subscribe((results: any) => {
  const [first, second, third] = results;

  const usersResponse = first.response;
  const appsResponse = second.response;
  const devicesResponse = third.response;

  Object.keys(devicesResponse).forEach((key) => {
    let dataDevices = devicesResponse[key].id;
    Object.assign(devicesResponse[key], {
      apps: appsResponse.filter(items => items.host_id == dataDevices)
    })
  })
  Object.keys(usersResponse).forEach((key) => {
    let dataUsers = usersResponse[key].id;
    Object.assign(usersResponse[key], {
      devices: devicesResponse.filter(items => items.user_id == dataUsers)
    })
  });

  const evaluatedDevices = devicesResponse.map((item) => {
    const size = item.apps.reduce((acuum, item) => {
      return acuum + item.size; 
    }, 0);

    const resultSize = Math.round(size / item.capacity * 100)

    return Object.assign({}, item, {
      procent: `${resultSize} %`
    })
  });

  
  console.log(usersResponse)
})


